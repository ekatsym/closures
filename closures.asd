(defsystem "closures"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "closures/tests"))))

(defsystem "closures/tests"
  :author ""
  :license ""
  :depends-on ("closures"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for closures"
  :perform (test-op (op c) (symbol-call :rove :run c)))
