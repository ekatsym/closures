(defpackage closures/tests/main
  (:use :cl
        :closures
        :rove))
(in-package :closures/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :closures)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
